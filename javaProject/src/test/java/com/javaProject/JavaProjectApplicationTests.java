package com.javaProject;

import com.javaProject.dao.PaymentDAO;
import com.javaProject.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.Assert.assertEquals;

@SpringBootTest
class JavaProjectApplicationTests {

	@Test
	void contextLoads() {
	}
	@Test
	void testPaymentConstructor(){
		// Date date = new GregorianCalendar(2019, 12, 15).getTime();
		Payment payment = new Payment(1,new Date(),"Card",3.99,1001);
		assertEquals("Card","Card",
				payment.getType());
	}

}
