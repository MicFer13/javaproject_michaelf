package com.javaProject.services;

import com.javaProject.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    void save (Payment payment);
}
