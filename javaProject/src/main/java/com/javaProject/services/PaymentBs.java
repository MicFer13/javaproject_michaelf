package com.javaProject.services;

import com.javaProject.dao.PaymentDAO;
import com.javaProject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
//    • Create a Payment class as shown in the UML Diagram.  Implement as a Spring Bean.
//        • In each operation invoke the appropriate PaymentDAO method
//        • Add validation to findByID to validate that the id > 0.
//        • Add validation to findByType to validate that type is not null.
@Service
public class PaymentBs implements PaymentService {
    @Autowired
    PaymentDAO dao;
    @Override
    public int rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {
        if(id != 0) {
            return dao.findById(id);
        }
        else   return null;
    }

    @Override
    public List<Payment> findByType(String type) {

        List<Payment> payments = dao.findByType(type);
        if((payments == null)||(type.equals(null)))
        {
            return null;
        }
        return payments;
    }

    @Override
    public void save(Payment payment) {
        dao.save(payment);
    }
}
