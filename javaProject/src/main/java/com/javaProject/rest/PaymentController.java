package com.javaProject.rest;

import com.javaProject.entities.Payment;
import com.javaProject.services.PaymentService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping("/api")
public class PaymentController
{
    @Autowired
    PaymentService ps;
    @RequestMapping(value ="/status", method = RequestMethod.GET)
    public String getStatus()
    {
        return "Rest Api is running";
    }

    @RequestMapping(value = "/rowcount", method= RequestMethod.GET)
    public int rowcount(){

        return ps.rowCount();
    }
    @RequestMapping(value = "/findById/{id}", method= RequestMethod.GET)
    public Payment findById(@PathVariable("id") int id){

        return ps.findById(id);
    }
    @RequestMapping(value = "/findByType/{type}", method= RequestMethod.GET)
    public List<Payment> findByType(String type){

        return ps.findByType(type);
    }
    @RequestMapping(value = "/save", method= RequestMethod.POST)
    public void save(@RequestBody Payment payment){

        ps.save(payment);
    }
}
