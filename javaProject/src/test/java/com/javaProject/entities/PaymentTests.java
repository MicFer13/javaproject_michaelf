package com.javaProject.entities;



import org.junit.jupiter.api.Test;

import java.util.Date;


import static org.junit.Assert.assertEquals;
class PaymentTest {

	@Test
	void contextLoads() {
	}
	@Test
	void testPaymentConstructor(){
		// Date date = new GregorianCalendar(2019, 12, 15).getTime();
		Payment payment = new Payment(1,new Date(),"Card",3.99,1001);
		assertEquals("Card","Card",
				payment.getType());
	}

}