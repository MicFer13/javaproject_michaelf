package com.javaProject.entities;

import java.util.Date;

public class Payment{
    private int id;
    private Date paymentdate;
    private String type;
    private double amount;
    private int custId;
    public Payment(int id,  Date paymentdate, String type, double amount, int custId) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }
    public String toString(){
        return (id + " "+custId+ " "+ amount+ " "+
                type + " "+paymentdate);


    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }
}