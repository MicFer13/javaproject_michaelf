package com.javaProject.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import com.javaProject.dao.PaymentDAO;
import com.javaProject.entities.Payment;
import com.javaProject.services.PaymentService;

@RunWith(SpringRunner.class)
@WebMvcTest(PaymentController.class)
//@ContextConfiguration(classes={com.javaProject.AppConfig.class})
public class PaymentControllerTest {
    @Autowired
	private MockMvc mockMvc;
    @MockBean
	PaymentDAO dao;

	@MockBean
	PaymentService service;
	
	@Test
	public void testPaymentDAOAPIfindById()  {

		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/api/findById/1")
					.accept(MediaType.APPLICATION_JSON)
			).andExpect(status().isOk())  
			.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
			.andExpect(jsonPath("type", is("Card")));
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
    @Test
	public void testPaymentDAOAPIfindByType()  {

		try {
			this.mockMvc.perform(MockMvcRequestBuilders.get("/api/findByType/?type=Card")
					.header("headerKey","headerValue")
					.accept(MediaType.APPLICATION_JSON)
			).andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
			.andExpect(MockMvcResultMatchers.jsonPath("$.type").exists());
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
    @Test
	public void testPaymentDAOAPIStatus()  {

		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/api/status")
					.header("headerKey","headerValue")
					.accept(MediaType.APPLICATION_JSON)
			).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Test
	public void testRowCount()  {

		try {
			mockMvc.perform(MockMvcRequestBuilders.get("/api/rowcount")
					.header("headerKey","headerValue")
					.accept(MediaType.APPLICATION_JSON)
			).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
