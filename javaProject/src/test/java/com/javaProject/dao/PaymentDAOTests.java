package com.javaProject.dao;
import com.javaProject.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentDAOTests {
    @Autowired
    PaymentDAO dao;
    @Test
    void testPaymentDAOsave(){

        Payment payment = new Payment(8,new Date(),"Card",3.99,1001);
        dao.save(payment);
        payment = dao.findById(1);
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOfindById(){

        Payment payment; //= new Payment(2,new Date(),"Card",4.99,1001);
        //dao.save(payment);
        payment = dao.findById(2);
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOFindByType(){

        List<Payment> payment;
        payment = dao.findByType("Card");
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOrowcount(){

        int rowCount = dao.rowCount();
        Assert.assertTrue(rowCount>1);
    }
}
