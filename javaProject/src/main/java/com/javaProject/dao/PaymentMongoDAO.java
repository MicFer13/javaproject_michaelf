package com.javaProject.dao;

import com.javaProject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PaymentMongoDAO implements PaymentDAO{

    @Autowired
    private MongoTemplate tpl;

    @Override
    public int rowCount() {
        Query query = new Query();
        long res  = tpl.count(query, Payment.class);
        // TODO Auto-generated method stub
        return ((int) res);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment= tpl.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment>  payment = tpl.find(query,Payment.class);
        return payment;
    }

    @Override
    public void save(Payment payment) {
        tpl.insert(payment);
    }
}
