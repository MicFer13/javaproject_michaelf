package com.javaProject;

import com.javaProject.entities.Payment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class JavaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaProjectApplication.class, args);
		Payment payment = new Payment(1,new Date(),"Card",3.99,1001);
		System.out.println(payment.getType());

	}

}
