package com.javaProject.services;
import com.javaProject.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentBsTest {
    @Autowired
    PaymentService ps;
    @Test
    void testPaymentDAOsave(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

        Payment payment = new Payment(1,date,"Card",3.99,1001);
        ps.save(payment);
        payment = ps.findById(1);
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOfindById(){

        Payment payment = new Payment(2,new Date(),"Card",4.99,1001);
        ps.save(payment);
        payment = ps.findById(2);
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOFindByType(){

        List<Payment> payment;
        payment = ps.findByType("Card");
        Assert.assertNotNull
                ("Not found",payment);
    }
    @Test
    void testPaymentDAOrowcount(){

        int rowCount = ps.rowCount();
        Assert.assertTrue(rowCount>1);
    }
}
