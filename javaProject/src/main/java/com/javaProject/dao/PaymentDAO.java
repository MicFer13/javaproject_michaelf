package com.javaProject.dao;

import com.javaProject.entities.Payment;

import java.util.List;

public interface PaymentDAO {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    void save (Payment payment);
}
